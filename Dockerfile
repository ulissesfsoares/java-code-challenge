FROM jboss/wildfly:10.1.0.Final

ADD dockerArtifacts/mysql-connector-java-5.1.39-bin.jar /opt/jboss/wildfly/modules/system/layers/base/com/mysql/driver/main/
ADD dockerArtifacts/module.xml /opt/jboss/wildfly/modules/system/layers/base/com/mysql/driver/main/
ADD dockerArtifacts/standalone.xml /opt/jboss/wildfly/standalone/configuration/standalone.xml
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0"]
ADD target/challenge-project.war /opt/jboss/wildfly/standalone/deployments/