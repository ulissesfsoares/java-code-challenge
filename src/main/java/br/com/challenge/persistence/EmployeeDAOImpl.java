package br.com.challenge.persistence;

import java.util.List;
import java.util.Objects;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.challenge.entity.Employee;
import br.com.challenge.exception.DAOException;

@Stateless
public class EmployeeDAOImpl {

	@PersistenceContext 
	private EntityManager entityManager;
	
	public List<Employee> getEmployees() {
		return entityManager.createNamedQuery(Employee.FIND_ALL_QUERY_NAME, Employee.class).getResultList();
	}
	
	public void insertEmployee(Employee employee) {
		entityManager.persist(employee);
	}
	
	public Employee updateEmployee(Employee employee) {
		return entityManager.merge(employee);
	}
	
	public Employee findEmployee(String email) {
		return entityManager.find(Employee.class, email);
	}
	
	public void deleteEmployee(String email) throws DAOException {
		Employee employee = findEmployee(email);
		
		if(Objects.isNull(employee))
			throw new DAOException("Not found");
		
		entityManager.remove(employee);
	}
}
