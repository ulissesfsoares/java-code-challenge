package br.com.challenge.resource;

import java.util.List;
import java.util.Objects;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.challenge.entity.Employee;
import br.com.challenge.exception.DAOException;
import br.com.challenge.persistence.EmployeeDAOImpl;

@Stateless
@Path("/employee")
public class EmployeeResource {

	@EJB
	EmployeeDAOImpl employeeDAO;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response processGetRequest() {
		
		List<Employee> employees = employeeDAO.getEmployees();
		return Response.status(Status.OK).entity(employees).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response processPostRequest(Employee employee) {
		Boolean existsEmployee = Objects.nonNull(employeeDAO.findEmployee(employee.getEmail()));
		if(existsEmployee)
			return Response.status(Status.BAD_REQUEST).entity(generateErrorObject("Duplicated resource. Email must be unique")).build();
		employeeDAO.insertEmployee(employee);
		return Response.status(Status.CREATED).entity(employee).build();
	}
	
	@PUT
	@Path("{email}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response processPutRequest(@PathParam("email") String email, Employee employee) {
		if(!employee.getEmail().equals(email))
			return Response.status(Status.BAD_REQUEST).entity(generateErrorObject("Email in path parameter and data field must not differ")).build();
		
		Boolean existsEmployee = Objects.nonNull(employeeDAO.findEmployee(email));
		employee = employeeDAO.updateEmployee(employee);
		
		Status returnStatus = existsEmployee ? Status.OK : Status.CREATED;
		return Response.status(returnStatus).entity(employee).build();
	}
	
	@DELETE
	@Path("{email}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response processDeleteRequest(@PathParam("email") String email) {
		try {
			employeeDAO.deleteEmployee(email);
			return Response.status(Status.OK).build();
		} catch (DAOException e) {
			return Response.status(Status.NOT_FOUND).build();
		}
		
	}
	
	private JsonObject generateErrorObject(String errorMessage) {
		JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add("error", errorMessage);
		return builder.build();
	}
	
}
