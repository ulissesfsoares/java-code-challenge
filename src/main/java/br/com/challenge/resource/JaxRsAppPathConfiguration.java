package br.com.challenge.resource;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rest")
public class JaxRsAppPathConfiguration extends Application {
    public JaxRsAppPathConfiguration() {
    	
	}
}
