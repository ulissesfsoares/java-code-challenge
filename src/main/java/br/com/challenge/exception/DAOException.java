package br.com.challenge.exception;

public class DAOException extends Exception {

	private static final long serialVersionUID = -3502497632911108525L;
	
	private final String errorMessage;

	public DAOException(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
