package br.com.challenge.exception;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ExceptionToJsonMapper implements ExceptionMapper<Exception>{

	@Override
	public Response toResponse(Exception e) {
		return Response.status(Status.BAD_REQUEST).entity(generateErrorObject(e.getMessage())).build();
	}
	
	private JsonObject generateErrorObject(String errorMessage) {
		JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add("error", errorMessage);
		return builder.build();
	}
}
