package br.com.challenge.validation;

import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NotEmptyValidator implements ConstraintValidator<NotEmpty, CharSequence> {

	private boolean trimValues;
	
	public NotEmptyValidator() {
	}

	public void initialize(NotEmpty annotation) {
		trimValues = annotation.trimValues();
	}

	public boolean isValid(CharSequence value, ConstraintValidatorContext context) {
		if (Objects.isNull(value) || (trimValues && value.toString().trim().isEmpty()) || value.length() == 0) {
			return false;
		}
		
		return true;
	}
	
}
