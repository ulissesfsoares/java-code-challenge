package br.com.challenge.presentation;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.challenge.entity.Employee;
import br.com.challenge.exception.DAOException;
import br.com.challenge.persistence.EmployeeDAOImpl;

@ManagedBean
@ViewScoped
public class EmployeeController implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@EJB 
	private EmployeeDAOImpl employeeDAO;
	
	private Employee employee;
	private List<Employee> employees;
	
	@PostConstruct
	public void init() {
		employees = employeeDAO.getEmployees();
		employee = new Employee();
	}

	public void addEmployee() {
		employeeDAO.insertEmployee(employee);
		this.employee = new Employee();
		employees = employeeDAO.getEmployees();
	}
	
	public void updateEmployee(Employee employee) {
		employeeDAO.updateEmployee(employee);
	}
	
	public void deleteEmployee(Employee employee) {
		try {
			employeeDAO.deleteEmployee(employee.getEmail());
			employees = employeeDAO.getEmployees();
		} catch (DAOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void test() {
		System.out.println("test");
	}
	
	public void test2(String testString) {
		System.out.println("test: " + testString);
	}
	
	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public List<Employee> getEmployee() {
		return Arrays.asList(employee);
	}

	public void setEmployee(List<Employee> employee) {
		this.employee = employee.get(0);
	}
	
	
}
