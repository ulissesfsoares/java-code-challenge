package br.com.challenge.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.commons.io.output.ThresholdingOutputStream;

import br.com.challenge.validation.NotEmpty;

@Entity
@Table(name = "employeeApp_employee")
@NamedQueries({
	@NamedQuery(name = Employee.FIND_ALL_QUERY_NAME, query = "select e from Employee e")
})
public class Employee {
	
	private String name;
	private String email;
	private String department;
	
	public final static String FIND_ALL_QUERY_NAME = "Employee.findAllQuery";
	
	@Column(name = "name", nullable = false)
	@NotEmpty
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Id
	@Column( name = "email", unique = true, updatable = false, nullable = false)
	@NotEmpty
	public String getEmail() {
		return email;
	}
	
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name = "department", nullable = false)
	@NotEmpty
	public String getDepartment() {
		return department;
	}
	
	public void setDepartment(String department) {
		this.department = department;
	}
	
}
