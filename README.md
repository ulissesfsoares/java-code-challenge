Java Maven project, usando wilfly 10.1.0 e mysql como base de dados.

- Executar scrit de inicialização do banco de dados: sql/create-mysql-db.sql
- Adicionar modulo do mysql ao Wildfly caso não houver (copiar or arquivos module e mysql-connector-java-5.1.39-bin.jar para wildfly-10.1.0.Final/modules/system/layers/base/com/mysql/driver/main)
- Trocar o arquivo de configuração do wildfly (wildfly-10.1.0.Final/standalone/configuration/standalone.xml) pelo arquivo dockerArtifacts/standalone.xml (esse arquivo já esta configurado com o datasource do banco de dados mysql)

A URL de de recurso para teste esta configurada como http://localhost:8000/rest/employee
A URL da interface gráfica de gerenciamento de funcionários é http://localhost:8000/pages/employee.jsf